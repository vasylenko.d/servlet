#Servlet: training project

####For cloning repo by 'https', please enter to terminal:
    $git clone https://gitlab.com/vasylenko.d/servlet.git

№1 CRUD app | servlet-filter | HttpSession
----------------
Branch: '01.crud'

№2 Authorization and authentication
----------------
Branch: '02.auth'

№3 Testing of servlet with Mockito
----------------
Branch: '03.test'